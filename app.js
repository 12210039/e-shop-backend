const express = require("express");
const app = express();

const categoryRouter = require("./routes/categoryRoutes");
const productRouter = require("./routes/productRoutes");
const userRouter = require("./routes/userRoutes");
const errorHandler = require("./helpers/error-handler");
const authJwt = require("./helpers/jwt");
const api = process.env.API_URL;

const orderRouter = require("./routes/orderRoutes");

//Middleware
app.use(express.json());
app.use("/public/uploads", express.static(__dirname + "/public/uploads"));
//Routes
app.use(`${api}/order`, orderRouter);
app.use(`${api}/category`, categoryRouter);
app.use(`${api}/product`, productRouter);
app.use(`${api}/user`, userRouter);
app.use(authJwt());
app.use(errorHandler);

module.exports = app;
